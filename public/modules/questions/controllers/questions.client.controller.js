'use strict';

// Questions controller
angular.module('questions').controller('QuestionsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Questions', 'Types', 'Technologies',
	function($scope, $stateParams, $location, Authentication, Questions, Types, Technologies) {
		$scope.authentication = Authentication;
		$scope.scores = [1,2,3,4,5,6,7,8,9,10];
		$scope.difficultyLevels = Types.query({property: 'difficultyLevel'});
		$scope.types = Types.query({property: 'type'});
		$scope.singleChoiceAnswers = [{isCorrect:false, text:''},{isCorrect:false, text:''}];
		$scope.multipleChoiceAnswers = [{isCorrect:false, text:''},{isCorrect:false, text:''}];

		// Create new Question
		$scope.create = function() {
			var technologyId = this.technology ? this.technology._id : undefined;
			// Create new Question object
			var question = new Questions ({
				question: this.question,
				technology: technologyId,
				score: this.score,
				difficultyLevel: this.difficultyLevel,
				type: this.type,
				answers: $scope.loadAnswer()
			});

			// Redirect after save
			question.$save(function(response) {
				$location.path('questions/' + response._id);

				// Clear form fields
				$scope.question = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Question
		$scope.remove = function( question ) {
			if ( question ) { question.$remove();

				for (var i in $scope.questions ) {
					if ($scope.questions [i] === question ) {
						$scope.questions.splice(i, 1);
					}
				}
			} else {
				$scope.question.$remove(function() {
					$location.path('questions');
				});
			}
		};

		// Update existing Question
		$scope.update = function() {
			var question = $scope.question;
			question.technology = question.technology._id;

			question.$update(function() {
				$location.path('questions/' + question._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Questions
		$scope.find = function() {
			$scope.questions = Questions.query();
		};

		// Find a list of Technologies
		$scope.findTechnologies = function() {
			$scope.technologies = Technologies.query();
		};

		// Find a list of Technologies
		$scope.findDataEditMode = function() {
			$scope.technologies = Technologies.query(function(err,obj) {
				$scope.question = Questions.get({
					questionId: $stateParams.questionId
				}, function(err,obj) {
					for (var i = 0; i < $scope.technologies.length; i++) {
						if ($scope.question.technology === $scope.technologies[i]._id) {
							$scope.question.technology = $scope.technologies[i];
							return;
						}
					}
					/*var id = $scope.question.technology;
					$scope.question.technology = window._.find($scope.technologies, function(tech) {
						return tech._id === id;
					});*/
				});
			});
		};

		// Find existing Question
		$scope.findOne = function() {
			$scope.question = Questions.get({
				questionId: $stateParams.questionId
			});
		};

		// Answers
		$scope.addAnswer = function(answers) {
			answers.push({isCorrect:false, text:''});
		};
		$scope.addAnswerForType = function(type) {
			if (type === $scope.types[0]) {
				$scope.addAnswer($scope.singleChoiceAnswers);
			} else {
				$scope.addAnswer($scope.multipleChoiceAnswers);
			}
		};

		$scope.removeAnswer = function(answers) {
			if (answers.length > 2) {
				answers.pop();
			}
		};
		$scope.removeAnswerForType = function(type) {
			if (type === $scope.types[0]) {
				$scope.removeAnswer($scope.singleChoiceAnswers);
			} else {
				$scope.removeAnswer($scope.multipleChoiceAnswers);
			}
		};

		$scope.loadAnswer = function() {
			//single choice answer
			if ($scope.type === $scope.types[0]) {
				return $scope.singleChoiceAnswers;
			}
			if ($scope.type === $scope.types[1]) {
				return $scope.multipleChoiceAnswers;
			}
			if ($scope.type === $scope.types[2]) {
				return [{isCorrect:true, text:$scope.keywordBasedAnswers}];
			}
		};

		// when modifying type of questions the array of answers needs to be erased
		$scope.setAnswers = function(answers) {
			answers.length = 0;
			$scope.addAnswer(answers);
			// if is single or multiple choice must contain at least 2 answers
			if ($scope.question.type !== $scope.types[2]){
				$scope.addAnswer(answers);
			}
		};
	}
]);