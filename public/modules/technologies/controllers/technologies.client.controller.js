'use strict';

// Technologies controller
angular.module('technologies').controller('TechnologiesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Technologies', 'Questions',
	function($scope, $stateParams, $location, Authentication, Technologies, Questions) {
		$scope.authentication = Authentication;

		// Create new Technology
		$scope.create = function() {
			// Create new Technology object
			var tags = this.tags || '';
			var technology = new Technologies ({
				name: this.name,
				description: this.description,
				tags: tags.split(',')
			});

			// Redirect after save
			technology.$save(function(response) {
				$location.path('technologies/' + response._id);

				// Clear form fields
				$scope.name = '';
				$scope.description = '';
				$scope.tags = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Technology
		$scope.remove = function( technology ) {
			if ( technology ) { technology.$remove();

				for (var i in $scope.technologies ) {
					if ($scope.technologies [i] === technology ) {
						$scope.technologies.splice(i, 1);
					}
				}
			} else {
				$scope.technology.$remove(function() {
					$location.path('technologies');
				});
			}
		};

		// Update existing Technology
		$scope.update = function() {
			var technology = $scope.technology;
			var tags = technology.tags || '';
			technology.tags = tags.split(',');

			technology.$update(function() {
				$location.path('technologies/' + technology._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Technologies
		$scope.find = function() {
			$scope.technologies = Technologies.query();
		};

		// Find existing Technology
		$scope.findOne = function() {
			$scope.technology = Technologies.get({technologyId: $stateParams.technologyId});
		};

		$scope.findOneEditMode = function() {
			Technologies.get({technologyId: $stateParams.technologyId}, function(obj) {
				obj.tags = obj.tags.join();
				$scope.technology = obj;
			});
		};

		// Find a list of Questions for the Technology
		$scope.findQuestions = function() {
			$scope.questions = Questions.query({technologyId: $stateParams.technologyId});
		};
	}
]);