'use strict';

// Exams controller
angular.module('exams').controller('ExamsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Exams', 'Types', 'Technologies', 'Questions',
	function($scope, $stateParams, $location, Authentication, Exams, Types, Technologies, Questions) {
		$scope.authentication = Authentication;
		$scope.difficultyLevels = Types.query({property: 'difficultyLevel'});
		$scope.numbers = [];

		// Create new Exam
		$scope.create = function() {
			var technologyId = this.technology ? this.technology._id : undefined;
			// Create new Exam object
			var exam = new Exams ({
				technology: technologyId,
				email: this.email,
				basicNumberOfQuestions: $scope.numbers.Basic,
				intermediateNumberOfQuestions: $scope.numbers.Intermediate,
				highNumberOfQuestions: $scope.numbers.High
			});

			// Redirect after save
			exam.$save(function(response) {
				$location.path('exams/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Exam
		$scope.remove = function(exam) {
			if ( exam ) {
				exam.$remove();

				for (var i in $scope.exams) {
					if ($scope.exams [i] === exam) {
						$scope.exams.splice(i, 1);
					}
				}
			} else {
				$scope.exam.$remove(function() {
					$location.path('exams');
				});
			}
		};

		// Update existing Exam
		$scope.update = function() {
			var exam = $scope.exam;
			exam.technology = exam.technology._id;
			exam.basicNumberOfQuestions = $scope.numbers.Basic;
			exam.intermediateNumberOfQuestions = $scope.numbers.Intermediate;
			exam.highNumberOfQuestions = $scope.numbers.High;

			exam.$update(function() {
				$location.path('exams/' + exam._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Exams
		$scope.find = function() {
			$scope.exams = Exams.query(
				function(err, obj) {
					for (var i = 0; i < $scope.exams.length; i++) {
						$scope.exams[i].technology = Technologies.get({technologyId: $scope.exams[i].technology});
					}
				});
		};

		// Load Exam and questions
		$scope.loadExam = function() {
			$scope.questionsForExam = [];
			$scope.exam = Exams.get({
				examId: $stateParams.examId
			}, function(err,obj) {
				$scope.technology = Technologies.get({technologyId: $scope.exam.technology});
				for (var i = 0; i < $scope.exam.questions.length; i++) {
					$scope.questionsForExam.push(Questions.get({questionId: $scope.exam.questions[i]}));
				}
			});
		};

		// Get the list of Technologies
		$scope.findTechnologies = function() {
			$scope.technologies = Technologies.query();
		};

		// Find a list of Technologies
		$scope.findDataEditMode = function() {
			$scope.technologies = Technologies.query(function(err,obj) {
				$scope.exam = Exams.get({
					examId: $stateParams.examId
				}, function(err,obj) {
					$scope.numbers.Basic = $scope.exam.basicNumberOfQuestions;
					$scope.numbers.Intermediate = $scope.exam.intermediateNumberOfQuestions;
					$scope.numbers.High = $scope.exam.highNumberOfQuestions;
					for (var i = 0; i < $scope.technologies.length; i++) {
						if ($scope.exam.technology === $scope.technologies[i]._id) {
							$scope.exam.technology = $scope.technologies[i];
							return;
						}
					}
				});
			});
		};

		//Print exam to pdf
		$scope.download = function() {
			var questionsToPrint = [];
			for (var i = 0; i < $scope.questionsForExam.length; i++) {
				questionsToPrint.push( i + 1 + ')' + $scope.questionsForExam[i].question);
				if ($scope.questionsForExam[i].answers.length > 1) {
					questionsToPrint.push('Choices: ');
					for (var j = 0; j < $scope.questionsForExam[i].answers.length; j++) {
						questionsToPrint.push('- ' + $scope.questionsForExam[i].answers[j].text);
					}
				}
			}
			var docDefinition = {
				content: [
					'Technology: ' + $scope.technology.name,
					'Candidate Email: ' + $scope.exam.email,
					'Number of Basic Level Questions: ' + $scope.exam.basicNumberOfQuestions,
					'Number of Intermediate Level Questions: ' + $scope.exam.intermediateNumberOfQuestions,
					'Number of High Level Questions: ' + $scope.exam.highNumberOfQuestions,
					' ',
					'Questions: ',
					questionsToPrint
				]
			};
			window.pdfMake.createPdf(docDefinition).download($scope.exam.email + '-' + $scope.technology.name + '.pdf');
		};
	}
]);