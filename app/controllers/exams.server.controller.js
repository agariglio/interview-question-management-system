'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Exam = mongoose.model('Exam'),
	Question = mongoose.model('Question'),
	_ = require('lodash');

/**
 * Get random questions for a given technology and difficulty level
 */
var getRandomQuestions = function(technologyId, difficultyLevel, limit, callback, res) {
 	if (limit > 0) {
 	 	Question.count({technology:technologyId, difficultyLevel:difficultyLevel}, function(err, count) {
 	 		if(count < limit) {
 	 			return res.status(400).send({
 					message: 'There are only '+count+' '+difficultyLevel+' level questions in the database'
 				});
 	 		}
 	 		else {
 	 			Question.findRandom({technology:technologyId, difficultyLevel:difficultyLevel}, {_id:1})
 				.limit(limit)
 				.exec(callback);
 	 		}
 	 	});
 	 }
 	 else {
 	 	callback(null, []);
 	 }
 };

/**
 * Save an Exam
 */
var saveExam = function(exam, res) {
	exam.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(exam);
		}
	});
};

/**
 * Execute queries to get the random questions for every difficulty level
 */
var generateQuestions = function(exam, body, res) {
	var highQueryCallback = function(err, result) {
		exam.questions = exam.questions.concat(result);
		saveExam(exam, res);
	};

	var intermediateQueryCallback = function(err, result) {
		exam.questions = exam.questions.concat(result);
		getRandomQuestions(body.technology, 'High', body.highNumberOfQuestions, highQueryCallback, res);
	};

	var basicQueryCallback = function(err, result) {
		exam.questions = result;
		getRandomQuestions(body.technology, 'Intermediate', body.intermediateNumberOfQuestions, intermediateQueryCallback, res);
	};

	getRandomQuestions(body.technology, 'Basic', body.basicNumberOfQuestions, basicQueryCallback, res);
};

/**
 * Create a Exam
 */
exports.create = function(req, res) {
	var exam = new Exam(req.body);
	exam.user = req.user;

	generateQuestions(exam, req.body, res);
};

/**
 * Show the current Exam
 */
exports.read = function(req, res) {
	res.jsonp(req.exam);
};

/**
 * Update a Exam
 */
exports.update = function(req, res) {
	var exam = req.exam;

	// Generate the questions again if any number of questions was changed
	if (exam.basicNumberOfQuestions !== req.body.basicNumberOfQuestions ||
			exam.intermediateNumberOfQuestions !== req.body.intermediateNumberOfQuestions ||
			exam.highNumberOfQuestions !== req.body.highNumberOfQuestions) {
		exam = _.extend(exam, req.body);
		generateQuestions(exam, req.body, res);
	}
	else {
		exam = _.extend(exam, req.body);
		saveExam(exam, res);
	}
};

/**
 * Delete an Exam
 */
exports.delete = function(req, res) {
	var exam = req.exam ;

	exam.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(exam);
		}
	});
};

/**
 * List of Exams
 */
exports.list = function(req, res) {
	Exam.find().sort('-created').populate('user', 'displayName').exec(function(err, exams) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(exams);
		}
	});
};

/**
 * Exam middleware
 */
exports.examByID = function(req, res, next, id) {
	Exam.findById(id).populate('user', 'displayName').exec(function(err, exam) {
		if (err) return next(err);
		if (! exam) return next(new Error('Failed to load Exam ' + id));
		req.exam = exam ;
		next();
	});
};

/**
 * Exam authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	// TODO
	/*if (req.exam.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}*/
	next();
};
