'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	Technology = mongoose.model('Technology'),
	_ = require('lodash');

/**
 * Create a Technology
 */
exports.create = function(req, res) {
	var technology = new Technology(req.body);

	Technology.findOne({name: req.body.name}, function(err,obj) { 
		if (obj) {
			return res.status(400).send({
				message: 'Technology \''+ obj.name + '\' already exists'
			});
		}
		technology.user = req.user;
		technology.tags = _.chain(req.body.tags)
			.map(function(tag) {return tag.trim().toLowerCase();})
			.reject(_.isEmpty)
			.uniq()
			.sort()
			.value();
		technology.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(technology);
			}
		});
	});
};

/**
 * Show the current Technology
 */
exports.read = function(req, res) {
	res.jsonp(req.technology);
};

/**
 * Update a Technology
 */
exports.update = function(req, res) {
	var technology = req.technology;

	Technology.findOne({name: req.body.name}, function(err,obj) { 
		if ((obj) && (obj.id !== technology.id)) {
			return res.status(400).send({
				message: 'Technology \''+ obj.name + '\' already exists'
			});
		}
		technology = _.extend(technology , req.body);
		technology.tags = _.chain(req.body.tags)
			.map(function(tag) {return tag.trim().toLowerCase();})
			.reject(_.isEmpty)
			.uniq()
			.sort()
			.value();
		technology.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(technology);
			}
		});
	});
};

/**
 * Delete an Technology
 */
exports.delete = function(req, res) {
	var technology = req.technology ;

	technology.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(technology);
		}
	});
};

/**
 * List of Technologies
 */
exports.list = function(req, res) { Technology.find().sort('name').populate('user', 'displayName').exec(function(err, technologies) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(technologies);
		}
	});
};

/**
 * Technology middleware
 */
exports.technologyByID = function(req, res, next, id) { Technology.findById(id).populate('user', 'displayName').exec(function(err, technology) {
		if (err) return next(err);
		if (! technology) return next(new Error('Failed to load Technology ' + id));
		req.technology = technology ;
		next();
	});
};

/**
 * Technology authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	// TODO check for admin user
	/*if (req.technology.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}*/
	next();
};