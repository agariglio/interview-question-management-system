'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Exam Schema
 */
var ExamSchema = new Schema({
	technology: {
		type: Schema.ObjectId,
		required: 'Please select a Technology',
		ref: 'Technology'
	},
	email: {
		type: String,
		default: '',
		required: 'Please fill Candidate email',
		trim: true
	},
	basicNumberOfQuestions: {
		type: Number,
		min: [0, 'Please select a number of questions greater or equal than {MIN}'],
		default: 0
	},
	intermediateNumberOfQuestions: {
		type: Number,
		min: [0, 'Please select a number of questions greater or equal than {MIN}'],
		default: 0
	},
	highNumberOfQuestions: {
		type: Number,
		min: [0, 'Please select a number of questions greater or equal than {MIN}'],
		default: 0
	},
	questions: {
		type: [Schema.ObjectId],
		ref: 'Question'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Exam', ExamSchema);