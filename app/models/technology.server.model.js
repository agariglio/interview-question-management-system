'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var tagsValidator = function(val) {
	if (typeof val !== 'undefined' && val.length > 0) {
		return true;
	}
	return false;
};

/**
 * Technology Schema
 */
var TechnologySchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Technology name',
		trim: true
	},
	description: {
		type: String,
		default: '',
		required: 'Please fill Technology description',
		trim: true
	},
	tags: {
		type: [String],
		required: 'Please add at least one tag',
		validate: tagsValidator
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Technology', TechnologySchema);