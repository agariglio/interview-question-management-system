'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	random = require('mongoose-random'),
	Schema = mongoose.Schema;

/**
 * Question Schema
 */
var AnswerSchema = new Schema({
	text: {
		type: String,
		required: 'Please write an answer in all the answer fields'
	},
	isCorrect: {
		type: Boolean
	}
});
var QuestionSchema = new Schema({
	question: {
		type: String,
		default: '',
		required: 'Please fill Question',
		trim: true
	},
	technology: {
		type: Schema.ObjectId,
		required: 'Please select a Technology',
		ref: 'Technology'
	},
	difficultyLevel: {
		type: String,
		required: 'Please select a Difficulty Level',
		enum: ['Basic', 'Intermediate', 'High']
	},
	score: {
		type: Number,
		min: [1, 'Please select a score greater or equal than {MIN}'],
		max: [10, 'Please select a score lower or equal than {MAX}'],
		required: 'Please select a score'
	},
	type: {
		type: String,
		required: 'Please select a Question type',
		enum: ['Single Choice', 'Multiple Choice', 'Keyword Based']
	},
	answers: {
		type: [AnswerSchema]
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

QuestionSchema.plugin(random, { path: 'r' });

/*
QuestionSchema.path('answers').validate(function(value) {
  return value.length;
},'answers cannot be an empty array');
*/
//TODO add validation to check there is at least one correct answer

mongoose.model('Question', QuestionSchema);